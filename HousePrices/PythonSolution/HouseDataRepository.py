# Install Pre-reqs before starting...
# e.g. `pip install pandas` in terminal

import pandas as pd
import numpy as np


class HouseDataRepository:

    def __init__(self):
        print("Creating house repo")

    def getRawHousePrices(self):
        return pd.read_csv("https://download.mlcc.google.com/mledu-datasets/california_housing_train.csv", sep=",")

    def getRandomizedHousePrices(self):
        house_prices = self.getRawHousePrices()
        return house_prices.reindex(np.random.permutation(house_prices.index))
