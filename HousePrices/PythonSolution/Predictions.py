
from __future__ import print_function
from HouseDataRepository import HouseDataRepository

import math

from IPython import display
from matplotlib import cm
from matplotlib import gridspec
from matplotlib import pyplot as plt

import numpy as np
import pandas as pd
import random as random # used later to shuffle

from sklearn import metrics
import tensorflow as tf
from tensorflow.python.data import Dataset

class Predictions:

    def __init__(self):
        print("Creating predictions instance")

    def getPredictedHouseValues(self):
        test = HouseDataRepository()

        print("Randomized House Prices")
        print(test.getRandomizedHousePrices().describe())

    def train_model(
            self,
            learning_rate,
            steps,
            batch_size,
            training_examples,
            training_targets,
            validation_examples,
            validation_targets):
        """Trains a linear regression model of multiple features.

        In addition to training, this function also prints training progress information,
        as well as a plot of the training and validation loss over time.

        Args:
          learning_rate: A `float`, the learning rate.
          steps: A non-zero `int`, the total number of training steps. A training step
            consists of a forward and backward pass using a single batch.
          batch_size: A non-zero `int`, the batch size.
          training_examples: A `DataFrame` containing one or more columns from
            `california_housing_dataframe` to use as input features for training.
          training_targets: A `DataFrame` containing exactly one column from
            `california_housing_dataframe` to use as target for training.
          validation_examples: A `DataFrame` containing one or more columns from
            `california_housing_dataframe` to use as input features for validation.
          validation_targets: A `DataFrame` containing exactly one column from
            `california_housing_dataframe` to use as target for validation.

        Returns:
          A `LinearRegressor` object trained on the training data.
        """

        periods = 10
        steps_per_period = steps / periods

        # Create a linear regressor object.
        my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
        my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)
        linear_regressor = tf.estimator.LinearRegressor(
            feature_columns=construct_feature_columns(training_examples),
            optimizer=my_optimizer
        )

# TODO: Tie up input functions below....
        TODO = [];

        # 1. Create input functions.
        training_input_fn = TODO # YOUR CODE HERE
        predict_training_input_fn = TODO # YOUR CODE HERE
        predict_validation_input_fn = TODO # YOUR CODE HERE

        # Train the model, but do so inside a loop so that we can periodically assess
        # loss metrics.
        print("Training model...")
        print("RMSE (on training data):")
        training_rmse = []
        validation_rmse = []
        for period in range(0, periods):
            # Train the model, starting from the prior state.
            linear_regressor.train(
                input_fn=training_input_fn,
                steps=steps_per_period,
            )
            # 2. Take a break and compute predictions.
            training_predictions = TODO # YOUR CODE HERE
            validation_predictions = TODO # YOUR CODE HERE

            # Compute training and validation loss.
            training_root_mean_squared_error = math.sqrt(
                metrics.mean_squared_error(training_predictions, training_targets))
            validation_root_mean_squared_error = math.sqrt(
                metrics.mean_squared_error(validation_predictions, validation_targets))
            # Occasionally print the current loss.
            print("  period %02d : %0.2f" % (period, training_root_mean_squared_error))
            # Add the loss metrics from this period to our list.
            training_rmse.append(training_root_mean_squared_error)
            validation_rmse.append(validation_root_mean_squared_error)
        print("Model training finished.")

        # Output a graph of loss metrics over periods.
        plt.ylabel("RMSE")
        plt.xlabel("Periods")
        plt.title("Root Mean Squared Error vs. Periods")
        plt.tight_layout()
        plt.plot(training_rmse, label="training")
        plt.plot(validation_rmse, label="validation")
        plt.legend()

        return linear_regressor