import tensorflow as tf
import pandas as pd
from matplotlib import pyplot as plt

# Documentation
# Guide: https://towardsdatascience.com/hello-world-in-tensorflow-973e6c38e8ed

# Setup:
# (See documentation here: https://www.tensorflow.org/install/pip)
#  1. pip3 install -U pip virtualenv
#  2. virtualenv --system-site-packages -p python ./venv
#        - Must run as admin
# Nothing is working matching documentation... going to use online collabory platform for now...

# Error Fixes
# 1. Error: "ModuleNotFoundError: No module named 'tensorflow'"
#    Run the following: python -m pip install --upgrade https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-0.12.0-py3-none-any.whl


# Step 2
# read data from csv
train_data = pd.read_csv("TrainingData\iris_training.csv", names=['f1', 'f2', 'f3', 'f4', 'f5'])
test_data = pd.read_csv("TestData\iris_test.csv", names=['f1', 'f2', 'f3', 'f4', 'f5'])

train_data.describe()
test_data.describe()

# separate training data
train_x = train_data(['f1', 'f2', 'f3', 'f4'])
train_y = train_data.ix[:, 'f5']

# separate test data
test_x = test_data(['f1', 'f2', 'f3', 'f4'])
test_y = test_data.ix[:, 'f5']

# Step 3
# define the placeholders for inputs (X), outputs (Y) and define the weights and bias of network. Here we have
# 4 columns in inputs as the data set has 4 features and 3 columns in output to map 3 types of flowers. The shape of
# placeholders should satisfy that. Further, the shape of weight matrix has to be 4x3 and bias has to be a vector of
# 3 to map the input to output (No hidden layers).

# 4 features, and 3 columns - map to 3 types of flowers